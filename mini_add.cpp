#include <omp.h>
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <cmath>

#define THREADS_PER_TEAM 128
#define NUM_TEAMS 1600
#ifndef USE_VERSION
#define USE_VERSION 0
#endif
#define WIDTH 1024
#define DEPTH 1024
#define HEIGHT 1024
#define ITERATIONS 100
#define WARMUPS 20

int main() {
  typedef std::chrono::system_clock Clock;

  double      a[WIDTH][DEPTH][HEIGHT];
  double      b[WIDTH][DEPTH][HEIGHT];
  double      c[WIDTH][DEPTH][HEIGHT];
  double c_host[WIDTH][DEPTH][HEIGHT];
  
  std::cout << "Allocation complete" << std::endl;

  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < DEPTH; j++) {
      for (int k = 0; k < HEIGHT; k++) {
        a[i][j][k] = (double) (1.01011 + (i*j*k*0.12345));
        b[i][j][k] = (double) (1.10111 + (i*j*k*0.56789));
        c[i][j][k] = (double) 0.0;
        c_host[i][j][k] = (double) 0.0;
        for (int r = 0; r < 3; r++) {
          c_host[i][j][k] += a[i][j][k] + b[i][j][k];
        }
      }
    }
  }

  std::cout << "Initialization complete" << std::endl;

  auto tstart = Clock::now();
  double ttotal = 0.0;

#pragma omp target data map(to: a[0:WIDTH][0:DEPTH][0:HEIGHT], b[0:WIDTH][0:DEPTH][0:HEIGHT]) map(tofrom: c[0:WIDTH][0:DEPTH][0:HEIGHT]) map(from: tstart, ttotal)
  {
#if USE_VERSION == 0
    std::cout << "Number of teams = " << NUM_TEAMS << std::endl;
    std::cout << "Threads per team = " << THREADS_PER_TEAM << std::endl;
    
    for (int iter = 0; iter < WARMUPS+ITERATIONS; ++iter) {
      if (iter == WARMUPS) {
        tstart = Clock::now();
      }
#pragma omp target teams distribute num_teams(NUM_TEAMS) thread_limit(THREADS_PER_TEAM)
      for (int i = 0; i < WIDTH; ++i) {
#pragma omp parallel for collapse(2)
        for (int j = 0; j < DEPTH; ++j) {
          for (int k = 0; k < HEIGHT; ++k) {
            for (int r = 0; r < 3; ++r) {
              c[i][j][k] += a[i][j][k] + b[i][j][k];
            }
          }
        }
      }
    }

#elif USE_VERSION == 1
    std::cout << "Number of teams = " << NUM_TEAMS << std::endl;
    std::cout << "Threads per team = " << THREADS_PER_TEAM << std::endl;
    
    for (int iter = 0; iter < WARMUPS+ITERATIONS; ++iter) {
      if (iter == WARMUPS) {
        tstart = Clock::now();
      }
#pragma omp target teams num_teams(NUM_TEAMS) thread_limit(THREADS_PER_TEAM)
      {
#pragma omp parallel
        {
          int total_teams = omp_get_num_teams();
          int team_id = omp_get_team_num();
          int slices_per_team = (WIDTH + total_teams - 1) / total_teams;
          int istart = team_id * slices_per_team;
          if (istart > WIDTH) {
            istart = WIDTH;
          }
          int iend = istart + slices_per_team;
          if (iend > WIDTH) {
            iend = WIDTH;
          }

          for (int i = istart; i < iend; ++i) {
#pragma omp for collapse(2)
            for (int j = 0; j < DEPTH; ++j) {
              for (int k = 0; k < HEIGHT; ++k) {
                for (int r = 0; r < 3; ++r) {
                  c[i][j][k] += a[i][j][k] + b[i][j][k];
                }
              }
            }
          }
        }
      }
    }

#elif USE_VERSION == 2
    size_t num_work_items = WIDTH * DEPTH * HEIGHT;
    std::cout << "Number of teams = " << NUM_TEAMS << std::endl;
    std::cout << "Threads per team = " << THREADS_PER_TEAM << std::endl;
    
    for (int iter = 0; iter < WARMUPS+ITERATIONS; ++iter) {
      if (iter == WARMUPS) {
        tstart = Clock::now();
      }
#pragma omp target teams distribute parallel for num_teams(NUM_TEAMS) thread_limit(THREADS_PER_TEAM)
      for (int id = 0; id < num_work_items; ++id) {
        int i = id/(DEPTH*HEIGHT);             // calculate slice number
        if (i < WIDTH) {
          int j = (id%(DEPTH*HEIGHT))/HEIGHT;  // calculate column number
          int k = id%HEIGHT;                   // calculate row number
          for (int r = 0; r < 3; ++r) {
            c[i][j][k] += a[i][j][k] + b[i][j][k];
          }
        }
      }
    }

#endif

    ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();

  } // end omp target data map

  double secs = ttotal / 1.0e6;
  int errors = 0;

  for (int i = 0; i < WIDTH; ++i) {
    for (int j = 0; j < DEPTH; ++j) {
      for (int k = 0; k < HEIGHT; ++k) {
        if (std::fabs(c[i][j][k] - c_host[i][j][k]) > 0.0000001) {
          errors++;
        }
      }
    }
  }

  if (errors != 0) {
    std::cout << "errors = " << errors << ", verification FAILURE" << std::endl;
  } else {
    std::cout << "errors = " << errors << ", verification SUCCESS" << std::endl;
  }
  
  std::cout << "Execution time (s): " << secs << std::endl;

  return 0;
}
