# Vendor can be nvidia | intel | amd
ifndef VENDOR
  VENDOR = nvidia
endif
# Compiler can be clang | pgi | cray
ifndef COMPILER
  COMPILER = clang
endif
# USE_VERSION = 0 | 1 | 2 | 3
ifndef VERSION
  VERSION = 0
endif

DEFINES = -DUSE_VERSION=$(VERSION)

ifeq ($(VENDOR),nvidia)
  ifeq ($(COMPILER),pgi)
    CC = pgc++
    CFLAGS = -std=c++11 -fast -mp=gpu:cc70
  else ifeq ($(COMPILER),cray) # Cray 9.x or later
    CC = CC
    CFLAGS = -std=c++11 -O3 -fopenmp
  else ifeq ($(COMPILER),cray-classic) # Cray 9.x or later (classic version)
    CC = CC
    CFLAGS = -O3 -h omp
  else
    CC = clang++
    CFLAGS = -std=c++11 -O3 -g
    CFLAGS += -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda
  endif
else ifeq ($(VENDOR),intel)
  CC = icx
  CFLAGS = -fiopenmp -fopenmp-targets=spir64 -D__STRICT_ANSI__
else ifeq ($(VENDOR),amd)
  CC = clang++
  CFLAGS = -std=c++14 -O3
  CFLAGS += -fopenmp -fopenmp-targets=amdgcn-amd-amdhsa -Xopenmp-target=amdgcn-amd-amdhsa -march=gfx906
endif

bench_mini_add.exe: mini_add.cpp
	$(CC) $(CFLAGS) $(DEFINES) -o $@ $<

all: bench_mini_add.exe

clean:
	rm -f *~ *.exe
